﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace LVL2_ASPNet_MVC_07.Controllers
{
    public class EmployeeController : Controller
    {
        private object viewer;

        // GET: Employee
        public ActionResult Index()
        {
            string ssrUrl = ConfigurationManager.AppSettings["SSRSReportUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            //report.SizeToReportContent = true;
            //report.AsyncRendering = true;
            report.ServerReport.ReportServerUrl = new Uri(ssrUrl);
            ReportParameter[] reportParameters = new ReportParameter[1];
            reportParameters[0] = new ReportParameter("keyword", "Ken");
            report.ServerReport.ReportPath = "/report";
            report.ServerReport.SetParameters(reportParameters);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }
    }
}